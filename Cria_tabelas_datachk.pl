#!/usr/bin/perl

  use DBI(); # Usa o pacote de banco de dados

  # Executa a conexão com o banco.
  #                          tipo       base a usar     endereço
  my $dbh = DBI->connect("DBI:mysql:database=datachk;host=localhost",
                         "root", "!P\@ssword", #login e senha
                         {'RaiseError' => 1});


#Cria a Tabela placaderede
#$dbh->do("CREATE TABLE placaderede (
#pdr_id SERIAL,
#fabricante VARCHAR(400),
#modelo VARCHAR(400))
#");
#print "Criada Tabela: placaderede\n";

#Cria a Tabela memoria
#$dbh->do("CREATE TABLE memoria (
#mem_id SERIAL,
#tipo VARCHAR(400),
#tamanho VARCHAR(200),
#fabricante VARCHAR(400),
#frequencia VARCHAR(100)
#)");
#print "Criada Tabela: memoria\n"; 

#Cria a Tabela processador
#$dbh->do("CREATE TABLE processador (
#proc_id SERIAL,
#tipo VARCHAR(400),
#velocidade VARCHAR(200),
#fabricante VARCHAR(400),
#socket VARCHAR(100)
#)");

#print "Criada Tabela: processador\n"; 

#Cria a Tabela placadevideo
#$dbh->do("CREATE TABLE placadevideo (
#pv_id SERIAL,
#memoria VARCHAR(400),
#processador VARCHAR(200),
#aceleradora3d VARCHAR(20),
#barramento VARCHAR(100)
#)");
#print "Criada Tabela: placadevideo\n"; 


#Cria a Tabela placamae
#$dbh->do("CREATE TABLE placamae (
#pm_id SERIAL,
#fabricante VARCHAR(400),
#modelo VARCHAR(200),
#socket VARCHAR(100),
#quantidadeMemoria VARCHAR(100),
#barramentoPCI VARCHAR(50),
#barramentoISA VARCHAR(50),
#barramentoAGP VARCHAR(50),
#interfaceIDE VARCHAR(50),
#interfaceSATA VARCHAR(50),
#interfaceSCSI VARCHAR(50),
#portaUSB VARCHAR(50),
#somOnBoard VARCHAR(50)
#)");
#print "Criada Tabela: placamae\n";

#Cria a Tabela fonte
#$dbh->do("CREATE TABLE fonte (
#fonte_id SERIAL,
#potencia VARCHAR(400),
#tipo VARCHAR(200),
#quantidadePinos VARCHAR(100)
#)");
#print "Criada Tabela: fonte\n"; 

#Cria a Tabela hd
#$dbh->do("CREATE TABLE hd (
#hd_id SERIAL,
#fabricante VARCHAR(400),
#tamanho VARCHAR(200),
#tipo VARCHAR(100)
#)");
#print "Criada Tabela: hd\n"; 


#Cria a Tabela driverOptico
#$dbh->do("CREATE TABLE driverOptico (
#driver_id SERIAL,
#fabricante VARCHAR(400),
#velocidade VARCHAR(200),
#tipo VARCHAR(100)
#)");
#print "Criada Tabela: driverOptico\n"; 


#Cria a Tabela disquete
$dbh->do("CREATE TABLE disquete (
disq_id SERIAL,
fabricante VARCHAR(400)
)");
print "Criada Tabela: disquete\n"; 

  # Desconecta do banco de dados
  $dbh->disconnect();
